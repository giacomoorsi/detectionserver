# coding=UTF-8
# from detect.py
from models import *
from utils.utils import *
from utils.datasets import *


# detection
import os
import sys
import time
import datetime
import argparse

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator


#server
import base64
import io
#import os
import pathlib
import sys
import tempfile



from flask import Flask
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for

from flask import jsonify

from flask_wtf.file import FileField
import numpy as np
from PIL import Image
from PIL import ImageDraw
from werkzeug.datastructures import CombinedMultiDict
from wtforms import Form
from wtforms import ValidationError



#APO
import torchvision.transforms.functional as TF

#from torchvision import accimage


app = Flask(__name__)

#import sys
#sys.path.append('/home/giacomo.orsi/PycharmProjects/pythonProject/darknet/python/')
#import darknet


content_types = {'jpg': 'image/jpeg',
                 'jpeg': 'image/jpeg',
                 'png': 'image/png'}
extensions = sorted(content_types.keys())


options = {
            "model_def": "ClothingDetection/yolov3-df2.cfg",
            "weights_path": "ClothingDetection/yolov3-df2_15000.weights",
            "class_path": "ClothingDetection/df2.names",
            "conf_thres": 0.2,  # object confidence threshold
            "nms_thres": 0.4,  # iov threshold for non-maximum suppression
            "batch_size": 1,  # size of the batches
            "n_cpu": 0,  # number of cpu threads to use during batch generation
            "img_size": 416  # size of each image dimension
}



def detect_objects(image_path):
    #image = Image.open(image_path).convert('RGB')
    image = Image.open(image_path)


    #image = accimage.open(image_path)


    app.logger.info("tipo immagine!!!")
    app.logger.info(type(image))
    #boxes, scores, classes, num_detections = \
    #    = client.detect(image)
    #image.thumbnail((480, 480), Image.ANTIALIAS)

    result = client.detect(image)


    #new_images = {}
    #for i in range(num_detections):
    #    if scores[i] < 0.7: continue
    #    cls = classes[i]
    #    if cls not in new_images.keys():
    #        new_images[cls] = image.copy()
       # draw_bounding_box_on_image(new_images[cls], boxes[i],
       #                            thickness=int(scores[i] * 10) - 4)

    #result = {}
    #result['original'] = encode_image(image.copy())

    #for cls, new_image in new_images.items():
    #    category = client.category_index[cls]['name']
    #    result[category] = encode_image(new_image)

    return result

@app.route('/', methods=['POST'])
def post():
    form = PhotoForm(CombinedMultiDict((request.files, request.form)))
    if request.method == 'POST' and form.validate():
        with tempfile.NamedTemporaryFile() as temp:
            form.input_photo.data.save(temp)
            temp.flush()
            result = detect_objects(temp.name)

        photo_form = PhotoForm(request.form)
        #return render_template('upload.html', photo_form=photo_form, result=result)
        #return jsonify(result)
        return str(result)
    else:
        return redirect(url_for('upload'))



class ObjectDetector() :

    def __init__(self):



        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        os.makedirs("output", exist_ok=True)
        self.model = Darknet(options["model_def"], img_size=options["img_size"]).to(self.device)

        if options["weights_path"].endswith(".weights"):
            # Load darknet weights
            self.model.load_darknet_weights(options["weights_path"])
        else:
            # Load checkpoint weights
            self.model.load_state_dict(torch.load(options["weights_path"]))

        self.model.eval()  # Set in evaluation mode

        fp = open(options["class_path"], "r")
        self.classes = fp.read().split("\n")[:-1]
        app.logger.info(self.classes)
        self.Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    def detect(self, image):

        prev_time = time.time()
        #input_img = Variable(image.type(self.Tensor))

        #input_img = np.array(image)
        #input_img = Variable(torch.Tensor(np.array(image)))
        #input_img = torch.Tensor(image)

        # versione 2
        #loader = transforms.Compose([
        #    transforms.ToTensor()])
        #image = loader(image).unsqueeze(0)
        #input_img = image.to(self.device, torch.float)

        # versione 3
        # input_img = TF.to_tensor(image)
        # input_img.unsqueeze_(0)
        # app.logger.info(input_img.shape)
        #
        # input_img = Variable(input_img.type(self.Tensor))
        #
        #versione 4
        img = image
        img_size = options["img_size"]
        ratio = min(img_size / img.size[0], img_size / img.size[1])
        imw = round(img.size[0] * ratio)
        imh = round(img.size[1] * ratio)
        img_transforms = transforms.Compose([transforms.Resize((imh, imw)),
                                             transforms.Pad((max(int((imh - imw) / 2), 0),
                                                             max(int((imw - imh) / 2), 0), max(int((imh - imw) / 2), 0),
                                                             max(int((imw - imh) / 2), 0)), (128, 128, 128)),
                                             transforms.ToTensor(),
                                             ])
        # convert image to Tensor
        image_tensor = img_transforms(img).float()
        image_tensor = image_tensor.unsqueeze_(0)
        input_img = Variable(image_tensor.type(self.Tensor))

        # Get detections
        with torch.no_grad():
            detections = self.model(input_img)
            detections = non_max_suppression(detections, options["conf_thres"], options["nms_thres"])

        # Log progress
        current_time = time.time()
        inference_time = datetime.timedelta(seconds=current_time - prev_time)
        #prev_time = current_time
        #print("\t+ Batch %d, Inference Time: %s" % (batch_i, inference_time))

        # detections_arr = []
        # detections_arr.extend(detections)
        #
        # input_arr = []
        # input_arr.extend(input_img)
        #
        # detections = zip(input_arr, detections_arr)


        #detections = slice(detections)
        #app.logger.info(detections)
        #out = str(type(detections[0]))  + "<br>"

        #out += str((detections[0].get_shape()))
        #  PREPARO OUTPUT

        detections = detections[0]

        if detections is not None:
            img = np.array(image)
            out = ""
            detections = rescale_boxes(detections, options["img_size"], img.shape[:2])
            unique_labels = detections[:, -1].cpu().unique()
            n_cls_preds = len(unique_labels)
            out += "N. unique labels: " + str(n_cls_preds) + "\n"

            i = 0
            output = []
            for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                entry = {"prediction": self.classes[int(cls_pred)], "x1": x1.item(), "y1": y1.item(), "x2" : x2.item(), "y2": y2.item(), "conf":conf.item()}
                output.append(entry)
        else :
            output =[]
            entry = {"prediction" : "none"}
            output.append(entry)

        return str(output)
        # Save image and detections
        #imgs.extend(img_paths)
        #img_detections.extend(detections)


def is_image():
    def _is_image(form, field):
        if not field.data:
            raise ValidationError()
        elif field.data.filename.split('.')[-1].lower() not in extensions:
            raise ValidationError()

    return _is_image

class PhotoForm(Form):
    input_photo = FileField(
        'File extension should be: %s (case-insensitive)' % ', '.join(extensions),
        validators=[is_image()])



@app.route('/')
def upload():
    photo_form = PhotoForm(request.form)
    return render_template('upload.html', photo_form=photo_form, result={}, options=options)

#
# @app.route('/post', methods=['GET', 'POST'])
# def post():
#     form = PhotoForm(CombinedMultiDict((request.files, request.form)))
#     if request.method == 'POST' and form.validate():
#         with tempfile.NamedTemporaryFile() as temp:
#             form.input_photo.data.save(temp)
#             temp.flush()
#             #result = detect_objects(temp.name)
#
#         photo_form = PhotoForm(request.form)
#         return render_template('upload.html',
#                                photo_form=photo_form, result=result)
#     else:
#         return redirect(url_for('upload'))


client = ObjectDetector()

# public service
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=50000, debug=False)
